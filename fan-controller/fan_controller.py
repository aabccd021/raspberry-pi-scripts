#!/usr/bin/env python3

import RPi.GPIO as GPIO
import time

check_temp_interval = 15
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(21, GPIO.OUT)
GPIO21 = 21

def get_temp():
    with open("/sys/class/thermal/thermal_zone0/temp", "r") as f:
        tmp = f.read()
        return int(tmp) / 1000

def run():
    print("run pwm fan")
    p = GPIO.PWM(GPIO21, 50)
    try:
        # fan_power 0% ~ 100%
        fan_power = 0
        while True:
            p.start(fan_power)
            temp = get_temp()
            print("temp:{}, fan_power:{}".format(temp, fan_power))
            if temp < 50:
                fan_power = 0
                check_temp_interval = 5
            elif temp < 57:
                fan_power = 25
                check_temp_interval = 10
            elif temp < 65:
                fan_power = 50
                check_temp_interval = 10
            elif temp < 75:
                fan_power = 75
                check_temp_interval = 15
            else:
                fan_power = 100
                check_temp_interval = 30
            p.ChangeDutyCycle(fan_power)

            time.sleep(check_temp_interval)
    except Exception as e:
        print(e)
    finally:
        p.stop()
        GPIO.cleanup()

if __name__ == "__main__":
    run()
